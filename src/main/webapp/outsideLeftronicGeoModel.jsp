<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>
<head>
    <title>Lefthronic Kbiz Example</title>
</head>
<body>
<h2>Lefthronic Geo Model Details</h2>
<form:form method="post" action="addGeoModel.html">

    <table>
        <tr>
            <td><form:label path="latitude">Latitude</form:label></td>
            <td><form:input path="latitude" /></td>
        </tr>
        <tr>
            <td><form:label path="longitude">Longitude</form:label></td>
            <td><form:input path="longitude" /></td>
        </tr>

        <tr>
            <td colspan="2">
                <input type="submit" value="Add GeoModel"/>
            </td>
        </tr>
    </table>

</form:form>

<form:form method="post" action="generateGeoModel.html">

    <table>

        <tr>
            <td colspan="2">
                <input type="submit" value="Generate Geo Model"/>
            </td>
        </tr>
    </table>

</form:form>

</body>
</html>