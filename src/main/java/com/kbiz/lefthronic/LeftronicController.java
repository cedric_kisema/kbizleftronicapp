package com.kbiz.lefthronic;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 */

@Controller
@SessionAttributes
public class LeftronicController {

    @RequestMapping(value = "/addGeoModel", method = RequestMethod.POST)
    public String addGeoData(@ModelAttribute("lefthronicGeoModel")
                                 LefthronicGeoModel lefthronicGeoModel, BindingResult result) {

        LeftronicHandler leftHandler = new LeftronicHandler();

        leftHandler.sendWorldMapGeoLocation(lefthronicGeoModel);

        System.out.println("Stream Name:" + lefthronicGeoModel.getStreamName() + "\n Longitude:" + lefthronicGeoModel.getLongitude() +
                "\n Latitude:" + lefthronicGeoModel.getLatitude());

        return "redirect:LefthronicPage.html";
    }

    @RequestMapping(value = "/generateGeoModel", method = RequestMethod.POST)
    public String generateGeoModel() {


        LeftronicHandler.generateWorldMapGeoLocation(5);

        return "redirect:GenerateLefthronicGeoModel.html";
    }

    @RequestMapping("/LefthronicPage")
    public ModelAndView showGeomap () {

        return new ModelAndView("outsideLeftronicGeoModel", "command", new LefthronicGeoModel());
    }

    @RequestMapping("/GenerateLefthronicGeoModel")
    public ModelAndView generateGeoModelView () {

        return new ModelAndView("generateLefthronicGeoModel", "command", new LefthronicGeoModel());
    }
}


