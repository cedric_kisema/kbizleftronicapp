package com.kbiz.lefthronic;

import com.kbiz.Util.MyRandomGenerator;

/**
 */

public class LefthronicGeoModel {

    private String streamName;
    private double longitude;
    private double latitude;

    public LefthronicGeoModel(String streamName, double longitude, double latitude) {

        this.streamName = streamName;
        this.longitude   = longitude ;
        this.latitude   = latitude  ;
    }

    public LefthronicGeoModel() {

//
//        PropertyReader pr = new PropertyReader();
//        Properties myProp = pr.readProperty();
//
//        String propertyStreamName = (String) myProp.get("geoModelStreamName");
        this.streamName = "iXSAE3wr";
        this.latitude   = MyRandomGenerator.generateRandomLatitude();
        this.longitude   = MyRandomGenerator.generateRandomLongitude() ;
    }


    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getStreamName() {
        return streamName;
    }
}
