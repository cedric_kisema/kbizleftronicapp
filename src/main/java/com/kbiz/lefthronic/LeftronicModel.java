package com.kbiz.lefthronic;

/**
 *
 */

public class LeftronicModel {

    private LefthronicGeoModel lefthronicGeoModel;

    public void setGeoModel(String streamName, double longitude, double latitude) {

        this.lefthronicGeoModel = new LefthronicGeoModel(streamName, longitude, latitude);
    }

    public LefthronicGeoModel getLefthronicGeoModel() {

        return lefthronicGeoModel;
    }


}
