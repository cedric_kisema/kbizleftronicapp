package com.kbiz.Util;

public class MyRandomGenerator {

    static double minLat = -90.00;
    static double maxLat = 90.00;
    static double minLon = 0.00;
    static double maxLon = 180.00;


    public static double generateRandomLatitude() {

        return  minLat + (double)(Math.random() * ((maxLat - minLat) + 1));
    }


    public static double generateRandomLongitude() {

        return minLon + (double)(Math.random() * ((maxLon - minLon) + 1));
    }
}