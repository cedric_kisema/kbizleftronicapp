package com.kbiz.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping ("/")
public class BaseController {

    @RequestMapping(value= "/welcome", method = RequestMethod.GET)
    public String welcome(ModelMap modelMap) {

        modelMap.addAttribute("message", "Welcome to my Maven Web Project");

        return "/index";

    }

    @RequestMapping (value="/welcome/{name}", method = RequestMethod.GET)
    public String welcomeName(@PathVariable String name, ModelMap model ) {

        return "/index";
    }

}
